package com.epam.cscalculator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class calculatorTest {

    @Test
    void testSum(){
        //arrange
        int n1 = 583; int n2 = 125;
        calculator calculator = new calculator();

        //act
        int act = calculator.sum(n1, n2);

        //assert
        assertEquals(708, act);
    }

    @Test
    void testSubtraction(){
        int n1 = 583; int n2 = 125;
        calculator calculator = new calculator();

        //act
        int act = calculator.sub(n1, n2);

        //assert
        assertEquals(458, act);
    }

    @Test
    void testMultiplication() {
        int n1 = 583; int n2 = 125;
        calculator calculator = new calculator();

        //act
        int act = calculator.mult(n1, n2);

        //assert
        assertEquals(72875, act);
    }

    @Test
    void testIntDivision() {
        int n1 = 583; int n2 = 125;
        calculator calculator = new calculator();

        //act
        int act = calculator.intDiv(n1, n2);

        //assert
        assertEquals(4, act);
    }

    @Test
    void testReminderDiv() {
        int n1 = 583; int n2 = 125;
        calculator calculator = new calculator();

        //act
        int act = calculator.remDiv(n1, n2);

        //assert
        assertEquals(83, act);
    }

}