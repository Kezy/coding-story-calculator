package com.epam.cscalculator;

import java.util.Scanner;

public class calculator {
    public int sum(int n1, int n2){
        return n1 + n2;
    }

    public int sub(int n1, int n2){
        return n1 - n2;
    }

    public int mult(int n1, int n2){
        return n1 * n2;
    }

    public int intDiv(int n1, int n2){
        return n1 / n2;
    }

    public int remDiv(int n1, int n2){
        return n1 % n2;
    }
}
